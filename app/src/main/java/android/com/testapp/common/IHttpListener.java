package android.com.testapp.common;

import org.json.JSONArray;

/**
 * Created by systems on 8/10/15.
 */
public interface IHttpListener {

    public void onSuccess (int serviceId, Object response);
    public void onFailure (int responseCode, Exception e);
}
