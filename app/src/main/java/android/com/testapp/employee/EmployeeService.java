package android.com.testapp.employee;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.com.testapp.common.IHttpListener;

/**
 * Created by systems on 8/10/15.
 */
public class EmployeeService {

    Context ctx;
    IHttpListener _listener;
    ProgressDialog progress;

    public EmployeeService(Context context) {
        this.ctx = context;
    }

    public void setListener(IHttpListener listener) {
        this._listener = listener;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void getEmployees() throws Exception {
        final ArrayList<Employee> list = new ArrayList<Employee>();

        progress = ProgressDialog.show(this.ctx, "Status",
                "Loading.. Please wait", true);

        if (isNetworkAvailable()) {
            Ion.with(this.ctx)
                    .load("https://s3-us-west-2.amazonaws.com/wirestorm/assets/response.json").asJsonArray().setCallback(new FutureCallback<JsonArray>() {
                @Override
                public void onCompleted(Exception e, JsonArray result) {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Employee>>() {
                    }.getType();
                    list.addAll((Collection<? extends Employee>) gson.fromJson(result, listType));

                    progress.dismiss();
                    _listener.onSuccess(1, list);
                }

            });
        } else {
            progress.dismiss();
            System.out.println("failure");
            _listener.onFailure(400, null);
        }


//        return list;
    }
}
