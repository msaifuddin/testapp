package android.com.testapp.employee;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import android.com.testapp.R;


public class DetailActivity extends ActionBarActivity {

    ProgressDialog  progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        progressDialog = ProgressDialog.show(this, "Status",
                "Loading.. Please wait", true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Intent in = getIntent();
        final Employee employeeObj = in.getExtras().getParcelable("employeeObj");

        ImageView imgv = (ImageView) findViewById(R.id.largeImage);

        Picasso.with(DetailActivity.this).load(employeeObj.getLrgpic()).placeholder(R.drawable.ic_launcher).into(imgv);

        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        finish();
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case  R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
