package android.com.testapp.employee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import android.com.testapp.R;

/**
 * Created by Administrator on 8/6/2015.
 */
public class EmployeeListAdapter extends ArrayAdapter<Employee> {

    private final Context context;
    private final ArrayList<Employee> values;

    public EmployeeListAdapter(Context context, ArrayList<Employee> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
            TextView tv_firstName = (TextView) rowView.findViewById(R.id.firstLine);
            TextView tv_secondName = (TextView) rowView.findViewById(R.id.secondLine);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            tv_firstName.setText(values.get(position).getName());
            tv_secondName.setText(values.get(position).getPosition());

            Picasso.with(context)
                .load(values.get(position).getSmallpic())
                    .placeholder(R.drawable.loading)
                .into(imageView);
            // change the icon for Windows and iPhone

            return rowView;

    }
}
