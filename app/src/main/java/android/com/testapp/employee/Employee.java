package android.com.testapp.employee;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 8/5/2015.
 */
public class Employee implements Parcelable {

    private String name ="";
    private String position ="";
    private String smallpic= "";
    private String lrgpic ="";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSmallpic() {
        return smallpic;
    }

    public void setSmallpic(String smallpic) {
        this.smallpic = smallpic;
    }

    public String getLrgpic() {
        return lrgpic;
    }

    public void setLrgpic(String lrgpic) {
        this.lrgpic = lrgpic;
    }

    protected Employee(Parcel in) {
        name = in.readString();
        position = in.readString();
        smallpic = in.readString();
        lrgpic = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(position);
        dest.writeString(smallpic);
        dest.writeString(lrgpic);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Employee> CREATOR = new Parcelable.Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}
