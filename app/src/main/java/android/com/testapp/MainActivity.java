package android.com.testapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import android.com.testapp.employee.DetailActivity;
import android.com.testapp.employee.Employee;
import android.com.testapp.employee.EmployeeListAdapter;
import android.com.testapp.employee.EmployeeService;
import android.com.testapp.R;


public class MainActivity extends android.com.testapp.BaseActivity {

    private ArrayList<Employee> employeeArrayList;
    EmployeeListAdapter adapter = null;
    ListView lv_employee;
    EmployeeService _service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _service = new EmployeeService(this);
        _service.setListener(this);

        lv_employee = (ListView) findViewById(R.id.list);

        Ion.getDefault(this).configure().setLogging("MyLogs", Log.DEBUG);

        try {
             _service.getEmployees();


        } catch (Exception e) {
            e.printStackTrace();
        }

        lv_employee.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent in = new Intent(MainActivity.this, DetailActivity.class);
                in.putExtra("employeeObj", employeeArrayList.get(position));
                startActivity(in);
            }
        });
    }



    @Override
    public void onSuccess(int serviceId, Object response) {
        employeeArrayList= (ArrayList<Employee>) response;

        adapter = new EmployeeListAdapter(this, employeeArrayList);
        lv_employee.setAdapter(adapter);

        for(Employee e : employeeArrayList){
            System.out.println("OnSuccess :"+e.getName());
        }
    }

    @Override
    public void onFailure(int responseCode, Exception e) {
        super.onFailure(responseCode, e);
    }
}
