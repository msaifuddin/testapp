package android.com.testapp;

import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import android.com.testapp.common.IHttpListener;

/**
 * Created by systems on 8/11/15.
 */
public class BaseActivity extends ActionBarActivity implements IHttpListener {

    @Override
    public void onSuccess(int serviceId, Object response) {

    }

    @Override
    public void onFailure(int responseCode, Exception e) {

        if(responseCode == 400){
            Toast.makeText(this, "Network Error ", Toast.LENGTH_SHORT);
        }
    }
}
